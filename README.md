<!-- ## 💻 Sobre a lib -->

<!-- O **formUI** e uma lib de componentes dinamicos de formulario em projeto PHP. -->

### 🎲 Instalação
    * Na raiz do projeto crie uma pasta ***libs-frontend*** e dentro uma pasta ***forms***
    * Copie o conteudo do arquivo formUI.php desse repositório e crie um arquivo com o mesmo nome
    * Cole o codigo e siga as orientações de utilização abaixo:
    * Exemplos: libs-frontend/forms/formUI.php

 <!-- * Na raiz do projeto crie um arquivo **composer.json**, com o seguinte conteudo:
 ```bash
    {
        "name": "cenario-dev/form-ui"
    }

 ``` -->
<!-- 
* Abra o projeto no terminal e execute o comando:
```bash
    composer install
```
* Agora faça a instalação da lib. Para isso execute no terminal:
```bash
    composer require cenario/form-validation
``` -->

### 🛠️ Usar
Utilize o require para importar a lib em sua pagina.
```bash
    <?php
        require_once __DIR__.'/libs-frontend/forms/formUI.php';
    ?>
```

### 🛠️ Gitignore
Adicione no gitignore
```bash
/libs-frontend
/libs-frontend/forms/*
```

Agora basta utilizar seguindo a documentação do notion

Notion formUI [https://bitter-ozraraptor-23c.notion.site/Componentes-70e4c7b5b0ab49a188792397b0c8262f]


## Settings

### 🛠️ Button link - Step 1
No step1 caso tenha um link que precisa ser habilitado somente se a validação de todos os componentes passarem, adicione em sua class="disabled" (configurando no css para o link ficar desabilitato) e um id conforme abaixo:
```bash
    <div>
        <a id='btn-avancar-01' class='salvar-session disabled' href='#'>
            Avançar
        <a/>
    <div/>
```
* Caso tenha dois links (mobile e desk), use: btn-avancar-01 e btn-avancar-02

### 🛠️ Retorno da validação
No step 2, na função de submit do formulario, faça a chamada da validação atribuindo em uma variavel e posteriormente utilize uma condicional travando a função:
```bash
    $(".salva-session").click(function(e) {
        e.preventDefault()

        const { isValidate, data } = inputValidateValue()
        console.log(data)

        if (!isValidate) {
            return
        }

        ...
        ...
        ...
    }
```

### 🛠️ Readme
Como nosso componente está no gitignore, precisamos deixar uma breve descrição caso haja um git clone futuro. Para isso adicione no Readme.md, conforme abaixo:
  ## Componentes do formulário
  * Na RAIZ do projeto crie uma pasta ***libs-frontend/forms***, dentro crie um arquivo ***formUI.php***
  * Entre no repositório abaixo, copie o conteudo e cole dentro do arquivo
  ```bash
      https://git-cenario.jelastic.saveincloud.net:4848/gabriel.xavier/form-validation-cenario/-/blob/master/formUI.php
  ```
  * Importe o componente na pagina
  ```bash
      require_once __DIR__.'/libs-frontend/forms/formUI.php';
  ```


### Validação do CEP
Quando seu formulario tiver um input do tipo CEP, adicione na função de submit, uma chamada para a função de validação, passando como parametro o id do cep

```bash
    const cep = document.getElementById('strCep').value

    let isValidForm = undefined

    if (!cep) {
        isValidForm = inputValidateValue('strCep')
    } else {
        isValidForm = inputValidateValue()
    }
    
    if (!isValidForm.isValidate) {
        return
    }
```
